import * as THREE from 'three'
import Experience from '../Experience.js'

export default class Goku
{
    constructor()
    {
        this.experience = new Experience()
        this.scene = this.experience.scene
        this.resources = this.experience.resources
        this.time = this.experience.time
        this.debug = this.experience.debug


        if(this.debug.active)
        {
            this.debugFolder = this.debug.ui.addFolder('goku')
        }


        this.resource = this.resources.items.gokuModel

        this.setModel()
        this.setAnimation()
    }

    setModel()
    {
        this.model = this.resource.scene
        this.scene.add(this.model)

        this.model.traverse((child) =>
        {
            if(child instanceof THREE.Mesh)
            {
                child.castShadow = true
            }
        })
    }

    setAnimation()
    {
        this.animation = {}
        
        this.animation.mixer = new THREE.AnimationMixer(this.model)
        
        this.animation.actions = {}
        
        this.animation.actions.kamehameha = this.animation.mixer.clipAction(this.resource.animations[0])
       
        
        this.animation.actions.current = this.animation.actions.kamehameha
        this.animation.actions.current.play()

        this.animation.play = (name) =>
        {
            const newAction = this.animation.actions[name]
            const oldAction = this.animation.actions.current
            newAction.reset()
            newAction.play()
            newAction.crossFadeFrom(oldAction, 1)

            this.animation.actions.current = newAction
        }


        if(this.debug.active)
        {
            const debugObject = {
                playKamehameha: () => { this.animation.play('kamehameha') },
            }
            this.debugFolder.add(debugObject, 'playKamehameha')
        }
    }

    update()
    {
        this.animation.mixer.update(this.time.delta * 0.001)
    }
}