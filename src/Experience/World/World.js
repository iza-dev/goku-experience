import Experience from '../Experience.js'
import Environment from './Environment.js'
import Floor from './Floor.js'
import Goku from './Goku.js'

export default class World
{
    constructor()
    {
        this.experience = new Experience()
        this.scene = this.experience.scene
        this.resources = this.experience.resources

        this.resources.on('ready', () =>
        {
            this.floor = new Floor()
            this.goku = new Goku()
            this.environment = new Environment()
        })
    }

    update()
    {
        if(this.goku)
            this.goku.update()
    }
}