import * as THREE from 'three'
import { THREEx } from '@ar-js-org/ar.js-threejs'

import Debug from './Utils/Debug.js'
import Sizes from './Utils/Sizes.js'
import Time from './Utils/Time.js'
import Camera from './Camera.js'
import Renderer from './Renderer.js'
import World from './World/World.js'
import Resources from './Utils/Resources.js'

import sources from './sources.js'

let instance = null

export default class Experience
{
    constructor(_canvas)
    {
        // Singleton
        if(instance)
        {
            return instance
        }
        instance = this
        
        // Global access
        window.experience = this

        // Options
        this.canvas = _canvas

        // Setup
        this.debug = new Debug()
        this.sizes = new Sizes()
        this.time = new Time()
        this.scene = new THREE.Scene()
        this.resources = new Resources(sources)
        this.camera = new Camera()
        this.renderer = new Renderer()
        this.world = new World()

        // Resize event
        this.sizes.on('resize', () =>
        {
            this.resize()
        })

        // Time tick event
        this.time.on('tick', () =>
        {
            this.update()
        })


      var arToolkitSource = new THREEx.ArToolkitSource({
      // to read from the webcam
      sourceType: 'webcam',
      sourceWidth: window.innerWidth,
      sourceHeight: window.innerWidth,
      // sourceWidth: window.innerWidth > window.innerHeight ? 640 : 480,
      // sourceHeight: window.innerWidth > window.innerHeight ? 480 : 640,
    })

     arToolkitSource.init(function onReady() {
      arToolkitSource.domElement.addEventListener('canplay', () => {
        console.log(
          'canplay',
          'actual source dimensions',
          arToolkitSource.domElement.videoWidth,
          arToolkitSource.domElement.videoHeight,
        );
        // this.initARContext();
        var  arToolkitContext = new THREEx.ArToolkitContext({
            cameraParametersUrl:  'data/camera_para.dat',
            detectionMode: 'mono',
        })
      arToolkitContext.init(() => {

      // this.camera.projectionMatrix.copy(arToolkitContext.getProjectionMatrix());

      // arToolkitContext.arController.orientation = getSourceOrientation();
      // arToolkitContext.arController.options.orientation = getSourceOrientation();

      console.log('arToolkitContext', arToolkitContext);
      window.arToolkitContext = arToolkitContext;
    })
      });
      window.arToolkitSource = arToolkitSource;
    //   setTimeout(() => {
    //     this.resize()
    //   }, 2000);

    }, function onError() { })
    }

   

   

    resize()
    {
        this.camera.resize()
        this.renderer.resize()
    }

    update()
    {
        this.camera.update()
        this.world.update()
        this.renderer.update()
    }

    destroy()
    {
        this.sizes.off('resize')
        this.time.off('tick')

        // Traverse the whole scene
        this.scene.traverse((child) =>
        {
            // Test if it's a mesh
            if(child instanceof THREE.Mesh)
            {
                child.geometry.dispose()

                // Loop through the material properties
                for(const key in child.material)
                {
                    const value = child.material[key]

                    // Test if there is a dispose function
                    if(value && typeof value.dispose === 'function')
                    {
                        value.dispose()
                    }
                }
            }
        })

        this.camera.controls.dispose()
        this.renderer.instance.dispose()

        if(this.debug.active)
            this.debug.ui.destroy()
    }
}